# Actis Leasing Parser

This package provides you with several useful python functions and also with CLI commands `alparser extract` and `alparser match`. Executions return:
```bash
$ alparser
Usage: alparser [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  extract
  match
```
```bash
$ alparser match --help
  Usage: alparser match [OPTIONS]

  Options:
    -f, --file TEXT            Path to document (PDF format)  [required]
    -d, --database TEXT        Database file (CSV format)  [required]
    -o, --output TEXT          Output file (JSON format)
    -c, --config TEXT          Path to config (YAML format)
    -n, --num-results INTEGER  Number of closest matches.
    --help                     Show this message and exit.
```
```bash
$ alparser extract --help
Usage: alparser extract [OPTIONS]

Options:
  -f, --file TEXT      Path to document (PDF format)  [required]
  -d, --database TEXT  Database file (CSV format)  [required]
  -o, --output TEXT    Output file (JSON format)
  -c, --config TEXT    Path to config (YAML format)
  --help               Show this message and exit.

```

The configuration file is located at the path: `/alparser/resources/config.yaml`

## Installation

### From .whl package

Install the program by executing:
```bash
pip install <package_name>.whl
```

### From source code

Install the package into your Python3 environment in editable mode by:
```bash
pip install -e .
```

## R&D installation

If you wish to develop the package and run tests install the package into your virtual environment by:
```bash
pip install -e .[test]
```

### Executing tests

Tests are executed by:
```bash
pytest .
```

### Pre-commit hooks

Pre-commit hooks substitute compilation in Python. You can install pre-commit git hooks defined in `.pre-commit-config.yaml` so that the code is linted before every commit and tests are run before every push:
```bash
pre-commit install && pre-commit install -t pre-push
```

### Creating wheel package

If you wish to create **wheel** package execute:
```bash
python setup.py bdist_wheel
```
The `.whl` package will be located in directory `/dist`.

### Versioning

Versioning is done using a [versioneer](https://github.com/warner/python-versioneer) tool. Versions are tagged by annotated git tags with prefix `v` in format:
```bash
git tag -a v1.4.2 -m "Release v1.4.2 with many new features."
```
Versioneer calculates version as number of commits from the last tagged version.
