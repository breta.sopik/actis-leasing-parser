import json
import os

import pytest

from alparser.parsing.document import doc_coords_iterator, doc_term
from alparser.utils import load_config
from alparser.types import Document, Config


resources_dir = os.path.join(os.path.dirname(__file__), "resources/")


def load_resource_doc(filename: str) -> Document:
    resource_path = os.path.join(resources_dir, filename)
    with open(resource_path) as file:
        doc = json.load(file)
    return doc


def coords_to_tuples(doc: Document) -> Document:
    for c in doc_coords_iterator(doc):
        term = doc_term(doc, c)
        if 'value_coord' in term:
            doc[c.page]['segments'][c.seg]['lines'][c.line]['terms'][c.term]['value_coord'] = tuple(term['value_coord'])
    return doc


@pytest.fixture
def config_test() -> Config:
    return load_config(os.path.join(resources_dir, "config_test.yaml"))


@pytest.fixture
def doc_labeling_vert_input() -> Document:
    return load_resource_doc("doc_labeling_vert_input.json")


@pytest.fixture
def doc_labeling_vert_output() -> Document:
    return coords_to_tuples(load_resource_doc("doc_labeling_vert_output.json"))


@pytest.fixture
def doc_labeling_horiz_input() -> Document:
    return load_resource_doc("doc_labeling_horiz_input.json")


@pytest.fixture
def doc_labeling_horiz_output() -> Document:
    return coords_to_tuples(load_resource_doc("doc_labeling_horiz_output.json"))


@pytest.fixture
def doc_labeling_prices_input() -> Document:
    return load_resource_doc("doc_labeling_prices_input.json")


@pytest.fixture
def doc_labeling_prices_output() -> Document:
    return coords_to_tuples(load_resource_doc("doc_labeling_prices_output.json"))


@pytest.fixture
def doc_matching_query_brand() -> Document:
    return coords_to_tuples(load_resource_doc("doc_matching_query_brand.json"))


@pytest.fixture
def doc_matching_query_price() -> Document:
    return coords_to_tuples(load_resource_doc("doc_matching_query_price.json"))


@pytest.fixture
def path_database() -> str:
    return os.path.join(resources_dir, "database.csv")


@pytest.fixture
def path_database_tiny() -> str:
    return os.path.join(resources_dir, "database_tiny.csv")


@pytest.fixture
def path_citroen() -> str:
    return os.path.join(resources_dir, "Citroen_20N66002265.pdf")


@pytest.fixture
def path_peugeot() -> str:
    return os.path.join(resources_dir, "Peugeot 2.pdf")
