from alparser.parsing.structure import belongs_to_line, pair_forms_term


def test_belongs_to_line_true():
    simple_line = {
        'id': 0,
        'x0': 100.,
        'x1': 110.,
        'top': 1.,
        'bottom': 11.,
        'words': []
    }
    word_on_line = {
        'x0': 200.,
        'x1': 210.,
        'top': 1.,
        'bottom': 11.9,
        'text': 'ahoj'
    }
    assert belongs_to_line(simple_line, word_on_line, epsilon=0.1) is True


def test_belongs_to_line_false():
    simple_line = {
        'id': 0,
        'x0': 100.,
        'x1': 110.,
        'top': 1.,
        'bottom': 11.,
        'words': []
    }
    word_off_line = {
        'x0': 200.,
        'x1': 210.,
        'top': 1.,
        'bottom': 10.0,
        'text': 'ahoj'
    }
    assert belongs_to_line(simple_line, word_off_line, epsilon=0.1) is False


def test_pair_forms_term_true():
    pair = ({
        'x0': 100.,
        'x1': 150.,
        'top': 1.,
        'bottom': 11.,
        'text': 'hello'
    }, {
        'x0': 160.,
        'x1': 200.,
        'top': 1.,
        'bottom': 11.,
        'text': 'kitty'

    })
    params = {
        'factor': 1.05,
        'colon_regex': r".+:$"
    }
    assert pair_forms_term(pair, params) is True


def test_pair_forms_term_false1():
    pair = ({
        'x0': 100.,
        'x1': 150.,
        'top': 1.,
        'bottom': 11.,
        'text': 'hello'
    }, {
        'x0': 180.,
        'x1': 200.,
        'top': 1.,
        'bottom': 11.,
        'text': 'kitty'
    })

    params = {
        'factor': 1.05,
        'colon_regex': r".+:$"
    }
    assert pair_forms_term(pair, params) is False


def test_pair_forms_term_false2():
    pair = ({
        'x0': 100.,
        'x1': 150.,
        'top': 1.,
        'bottom': 11.,
        'text': 'hello:'
    }, {
        'x0': 160.,
        'x1': 200.,
        'top': 1.,
        'bottom': 11.,
        'text': 'kitty'
    })
    params = {
        'factor': 1.05,
        'colon_regex': r".+:$"
    }
    assert pair_forms_term(pair, params) is False
