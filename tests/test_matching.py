import pandas as pd
from pandas.testing import assert_frame_equal

from alparser.matching.brand import query_brand
from alparser.matching.price import base_price_regex, base_price_all
from alparser.matching.model import get_keywords_idf_scores
from alparser.utils import load_database


def test_query_brand(doc_matching_query_brand, config_test):
    brands_actual = query_brand(doc_matching_query_brand, config_test['match']['brand'])
    brands_expected = "Škoda"
    assert brands_actual == brands_expected


def test_query_price_regex(doc_matching_query_price, config_test):
    prices_actual = base_price_regex(doc_matching_query_price, config_test['match']['price']['base_price_regexes'])
    prices_expected = [1e5]
    assert prices_actual == prices_expected


def test_query_price_all(doc_matching_query_price):
    prices_actual = base_price_all(doc_matching_query_price)
    prices_expected = [1e5, 2e5]
    assert prices_actual == prices_expected


def test_keywords_idf_scores(path_database_tiny, config_test):
    database = load_database(path_database_tiny)
    keywords_actual, idf_actual = get_keywords_idf_scores(database, config_test['match']['model_verze'])
    keywords_expected = pd.DataFrame([
        {"IDVERZE": 200351, "keyword": "Cayenne"},
        {"IDVERZE": 200351, "keyword": "Coupe"},
        {"IDVERZE": 200351, "keyword": "'20"},
        {"IDVERZE": 200351, "keyword": "5"},
        {"IDVERZE": 200351, "keyword": "dv."},
        {"IDVERZE": 200351, "keyword": "SUV"},
        {"IDVERZE": 200351, "keyword": "2.9"},
        {"IDVERZE": 200351, "keyword": "S"},
        {"IDVERZE": 200351, "keyword": "Tiptronic"},
        {"IDVERZE": 200351, "keyword": "Coupé"},
        {"IDVERZE": 195787, "keyword": "718"},
        {"IDVERZE": 195787, "keyword": "'19"},
        {"IDVERZE": 195787, "keyword": "2"},
        {"IDVERZE": 195787, "keyword": "dv."},
        {"IDVERZE": 195787, "keyword": "cabrio"},
        {"IDVERZE": 195787, "keyword": "2.0"},
        {"IDVERZE": 195787, "keyword": "Boxster"},
        {"IDVERZE": 169161, "keyword": "911"},
        {"IDVERZE": 169161, "keyword": "VII"},
        {"IDVERZE": 169161, "keyword": "'18"},
        {"IDVERZE": 169161, "keyword": "2"},
        {"IDVERZE": 169161, "keyword": "dv."},
        {"IDVERZE": 169161, "keyword": "cabrio"},
        {"IDVERZE": 169161, "keyword": "3.0"},
        {"IDVERZE": 169161, "keyword": "Carrera"},
        {"IDVERZE": 169161, "keyword": "GTS"},
        {"IDVERZE": 169161, "keyword": "Cabriolet"},
        {"IDVERZE": 200415, "keyword": "911"},
        {"IDVERZE": 200415, "keyword": "VIII"},
        {"IDVERZE": 200415, "keyword": "'20"},
        {"IDVERZE": 200415, "keyword": "2"},
        {"IDVERZE": 200415, "keyword": "dv."},
        {"IDVERZE": 200415, "keyword": "cabrio"},
        {"IDVERZE": 200415, "keyword": "3.0"},
        {"IDVERZE": 200415, "keyword": "Carrera"},
        {"IDVERZE": 200415, "keyword": "Cabriolet"}
    ])

    idf_expected = pd.DataFrame([
        {'keyword': 'Cayenne', 'idf_score': 1.386},
        {'keyword': 'Coupe', 'idf_score': 1.386},
        {'keyword': "'20", 'idf_score': 0.693},
        {'keyword': 'SUV', 'idf_score': 1.386},
        {'keyword': '2.9', 'idf_score': 1.386},
        {'keyword': 'Tiptronic', 'idf_score': 1.386},
        {'keyword': 'Coupé', 'idf_score': 1.386},
        {'keyword': '718', 'idf_score': 1.386},
        {'keyword': "'19", 'idf_score': 1.386},
        {'keyword': 'cabrio', 'idf_score': 0.288},
        {'keyword': '2.0', 'idf_score': 1.386},
        {'keyword': 'Boxster', 'idf_score': 1.386},
        {'keyword': '911', 'idf_score': 0.693},
        {'keyword': 'VII', 'idf_score': 1.386},
        {'keyword': "'18", 'idf_score': 1.386},
        {'keyword': '3.0', 'idf_score': 0.693},
        {'keyword': 'Carrera', 'idf_score': 0.693},
        {'keyword': 'GTS', 'idf_score': 1.386},
        {'keyword': 'Cabriolet', 'idf_score': 0.693},
        {'keyword': 'VIII', 'idf_score': 1.386}
    ])

    assert_frame_equal(keywords_actual.reset_index(drop=True), keywords_expected)
    assert_frame_equal(pd.DataFrame(idf_actual)[['keyword', 'idf_score']], idf_expected, check_less_precise=2)
