import pandas as pd
from pandas.testing import assert_series_equal, assert_frame_equal

from alparser.cli import _extract, _match


def test_extract_citroen(path_citroen, path_database, config_test):
    digest_actual = pd.Series(_extract(path_citroen, path_database, config_test))
    digest_expected = pd.Series({
        'ZNACKA': 'Citroen',
        'MODEL': "C4 Cactus '19 5 dv. hatchback",
        'VERZE': '1.2 PureTech 110 S&S Shine',
        'IDVERZE': 195702,
        'CENAZANOVY': 381736.0,
        'match score': 6.0,
        'base_price': 461900.0,
        'manu_price': 486900.0,
        'reduction': -111800.0,
        'final_price': 375100.0,
        'manu_extra': 25000.0,
        'dealer_extra': -0.01
    })
    assert_series_equal(digest_actual, digest_expected, check_less_precise=2)


def test_extract_peugeot(path_peugeot, path_database, config_test):
    digest_actual = pd.Series(_extract(path_peugeot, path_database, config_test))
    digest_expected = pd.Series({
        'ZNACKA': 'Peugeot',
        'MODEL': "308 II '20 5 dv. hatchback",
        'VERZE': 'ACTIVE 1.2 PureTech 130 S&S',
        'IDVERZE': 199793,
        'CENAZANOVY': 388429.75,
        'match score': 17.684,
        'base_price': 470000.0,
        'manu_price': 490500.0,
        'reduction': -93195.0,
        'final_price': 328351.24,
        'manu_extra': 20500.0,
        'dealer_extra': -68953.76
    })
    assert_series_equal(digest_actual, digest_expected, check_less_precise=2)


def test_match(path_peugeot, path_database, config_test):
    matches_actual = pd.DataFrame(_match(path_peugeot, path_database, config_test, 3))
    matches_expected = pd.DataFrame([
        {
            'ZNACKA': 'Peugeot',
            'MODEL': "308 II '20 5 dv. hatchback",
            'VERZE': 'ACTIVE 1.2 PureTech 130 S&S',
            'IDVERZE': 199793,
            'CENAZANOVY': 388429.75,
            'match score': 17.68
        },
        {
            'ZNACKA': 'Peugeot',
            'MODEL': "308 II '20 5 dv. kombi",
            'VERZE': 'SW ACTIVE 1.2 PureTech 110 S&S',
            'IDVERZE': 199798,
            'CENAZANOVY': 388429.75,
            'match score': 11.85
        },
        {
            'ZNACKA': 'Peugeot',
            'MODEL': "2008 '19 5 dv. SUV",
            'VERZE': 'ACTIVE 1.5 BlueHDi 100 S&S MAN6',
            'IDVERZE': 190201,
            'CENAZANOVY': 390082.64,
            'match score': 8.84
        }
    ])
    assert_frame_equal(matches_actual, matches_expected, check_less_precise=2)
