from alparser.parsing.labeling import label_vertical_key_values, label_horizontal_key_values, label_prices


def test_label_vertical_key_values(doc_labeling_vert_input, doc_labeling_vert_output, config_test):
    doc_labeling_actual = label_vertical_key_values(doc_labeling_vert_input, config_test['parse']['keywords'])
    assert doc_labeling_actual == doc_labeling_vert_output


def test_label_horizontal_key_values(doc_labeling_horiz_input, doc_labeling_horiz_output, config_test):
    doc_labeling_actual = label_horizontal_key_values(doc_labeling_horiz_input, config_test['parse']['keywords'])
    assert doc_labeling_actual == doc_labeling_horiz_output


def test_label_prices(doc_labeling_prices_input, doc_labeling_prices_output, config_test):
    doc_labeling_actual = label_prices(doc_labeling_prices_input, config_test['parse']['price'])
    assert doc_labeling_actual == doc_labeling_prices_output
