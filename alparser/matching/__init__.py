import pandas as pd

from .brand import match_brand
from .price import match_base_price, filter_base_price
from .model import match_model_verze
from ..types import MatchConfig, Document
from ..utils import logger


def match_database(document: Document,
                   database: pd.DataFrame,
                   config: MatchConfig,
                   results_num: int = None) -> pd.DataFrame:
    logger.info('Matching with database.')
    results_num = 1 if results_num is None else results_num
    matching_functions = [
        (match_base_price, config['price']),
        (match_brand, config['brand']),
        (match_model_verze, config['model_verze'])
    ]
    pdf_result = database.copy()
    for matching_func, matching_config in matching_functions:
        pdf_result = matching_func(document, pdf_result, matching_config)
    return pdf_result.drop_duplicates().reset_index(drop=True).iloc[:results_num]


def match_database_base_price(document: Document,
                              database: pd.DataFrame,
                              base_price: float,
                              config: MatchConfig) -> pd.DataFrame:
    matching_functions = [
        (match_brand, config['brand']),
        (match_model_verze, config['model_verze'])
    ]
    pdf_result = filter_base_price(database.copy(), [base_price], config['price']['rel_eps'])
    for matching_func, matching_config in matching_functions:
        pdf_result = matching_func(document, pdf_result, matching_config)
    return pdf_result.drop_duplicates().reset_index(drop=True).iloc[0]
