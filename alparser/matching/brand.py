import re
from collections import Counter
from typing import Dict, List

import pandas as pd

from ..parsing.document import doc_coords_iterator, doc_term
from ..types import Document


def get_max_count(hits: List[str]) -> str:
    counts = Counter(hits)
    return sorted(counts, key=counts.get, reverse=True)[0]


def query_brand(document: Document, dict_brands: Dict[str, List[str]]) -> str:
    brand_regexes = {b: [re.compile(fr"(^|.*\s){q}(\s.*|$)") for q in dict_brands[b]] for b in dict_brands.keys()}
    matches = []
    for coord in doc_coords_iterator(document):
        term = doc_term(document, coord)
        for brand_label, brand_regex in brand_regexes.items():
            brand_matches = [brand_label for r in brand_regex if r.match(term['text'].lower()) is not None]
            if len(brand_matches) > 0:
                matches += brand_matches
    if len(matches) == 0:
        raise ValueError('No brand found.')
    else:
        return get_max_count(matches)


def filter_brand(database: pd.DataFrame, brand: str) -> pd.DataFrame:
    return database[database['ZNACKA'] == brand]


def match_brand(document: Document, database: pd.DataFrame, dict_brands: Dict[str, List[str]]) -> pd.DataFrame:
    brand = query_brand(document, dict_brands)
    return filter_brand(database, brand)
