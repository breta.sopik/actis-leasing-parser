import re
from collections import Counter
from typing import Dict, List, Union

import numpy as np
import pandas as pd

from ..parsing.document import doc_coords_iterator, doc_term
from ..types import Document, KeywordsIDF


def get_keywords_idf_scores(database: pd.DataFrame,
                            params: Dict[str, Union[float, List[str]]]) -> (pd.DataFrame, KeywordsIDF):
    pdf_keywords = database[['IDVERZE', 'MODEL', 'VERZE']].copy()
    models_num = len(pdf_keywords)
    pdf_keywords['keyword'] = pdf_keywords.apply(lambda x: x['MODEL'].split() + x['VERZE'].split(), axis=1)
    pdf_keywords = pdf_keywords.explode('keyword')

    def filter_keywords(kw: str) -> bool:
        return (len(kw) > params['excluded_len']) and (kw not in params['excluded_kw'])

    keywords = Counter(filter(filter_keywords, pdf_keywords['keyword']))
    keywords_idf = [{
        'keyword': k,
        'regex': re.compile(fr"(^|.*\s){re.escape(k.lower())}(\s.*|$)"),
        'idf_score': np.log(models_num / v)
    } for k, v in keywords.items()]
    return pdf_keywords[['IDVERZE', 'keyword']], keywords_idf


def match_keywords(document: Document, keywords_idf: KeywordsIDF) -> pd.DataFrame:
    matches = []
    for kw in keywords_idf:
        for coord in doc_coords_iterator(document):
            term = doc_term(document, coord)
            if kw['regex'].match(term['text'].replace(',', '.').lower()) and term['type'] != 'price':
                matches.append({
                    'keyword': str(kw['keyword']),
                    'term': term['text'],
                    'term_coord': coord,
                    'idf_score': kw['idf_score']
                })
    return pd.DataFrame(matches)


def evaluate_matches(database: pd.DataFrame, pdf_keywords: pd.DataFrame, pdf_matches: pd.DataFrame) -> pd.DataFrame:
    pdf_idf = pd.merge(pdf_keywords, pdf_matches, on='keyword').groupby('IDVERZE').sum()
    pdf_results = pd.merge(database, pdf_idf, on='IDVERZE')
    return pdf_results.rename(columns={'idf_score': 'match score'}).sort_values(by='match score', ascending=False)


def match_model_verze(document: Document,
                      database: pd.DataFrame,
                      params: Dict[str, Union[float, List[str]]]) -> pd.DataFrame:
    pdf_keywords, keywords_idf = get_keywords_idf_scores(database, params)
    pdf_matches = match_keywords(document, keywords_idf)
    pdf_results = evaluate_matches(database, pdf_keywords, pdf_matches)
    return pdf_results
