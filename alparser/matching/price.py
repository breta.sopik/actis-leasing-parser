import re
from typing import Dict, List, Union

import pandas as pd

from ..parsing.document import doc_coords_iterator, doc_term, doc_line
from ..types import Document, DocCoord


def base_price_regex(doc: Document, regexes: Dict[str, List[str]]) -> List[float]:
    regexes_pos = [re.compile(r) for r in regexes['positive']]
    regexes_neg = [re.compile(r) for r in regexes['negative']]
    results = []
    for coord in doc_coords_iterator(doc):
        term = doc_term(doc, coord)
        matches_positive = [r for r in regexes_pos if r.match(term['text'].lower()) is not None]
        matches_negative = [r for r in regexes_neg if r.match(term['text'].lower()) is not None]
        if len(matches_positive) > 0 and len(matches_negative) == 0:
            if term['type'] == 'key':
                value_term = doc_term(doc, coord + DocCoord(*term['value_coord']))
                if value_term['type'] == 'price':
                    results.append(value_term['price'])
            else:
                for term_price in doc_line(doc, coord)['terms']:
                    if term_price['type'] == 'price':
                        results.append(term_price['price'])
    return results


def base_price_all(doc: Document) -> List[float]:
    results = []
    for coord in doc_coords_iterator(doc):
        term = doc_term(doc, coord)
        if term['type'] == 'price' and not term['price'] is None:
            results.append(term['price'])
    return results


def relat_diff(price: float, tab_price: float) -> float:
    return abs(price - tab_price) / tab_price


def filter_base_price(database: pd.DataFrame, prices: List[float], rel_eps: float) -> pd.DataFrame:
    pdf_filtered = database[database['CENAZANOVY'] > 0].copy()
    pdf_filtered['price_match'] = pdf_filtered['CENAZANOVY'] \
        .apply(lambda x: any(relat_diff(p, x) < rel_eps or relat_diff(p / 1.21, x) < rel_eps for p in prices))
    return pdf_filtered[pdf_filtered['price_match'] == True].drop(columns='price_match')


def match_base_price(document: Document,
                     database: pd.DataFrame,
                     params: Dict[str, Union[float, Dict[str, List[str]]]]) -> pd.DataFrame:
    base_price_matches = base_price_regex(document, params['base_price_regexes'])
    if len(base_price_matches) == 0:
        base_price_matches = base_price_all(document)
    return filter_base_price(database, base_price_matches, params['rel_eps'])
