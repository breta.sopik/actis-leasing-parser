import re
from typing import List, Dict, Tuple, Union

import pdfplumber

from ..types import DecWord, Word, Line, SimpleLine, Segment, Document, ParseConfig


def make_words_float(words: List[DecWord]) -> List[Word]:
    return [{
        'x0': float(w['x0']),
        'x1': float(w['x1']),
        'top': float(w['top']),
        'bottom': float(w['bottom']),
        'text': w['text']
    } for w in words]


def belongs_to_line(line: SimpleLine, word: Word, epsilon: float) -> bool:
    return abs(line['bottom'] - word['bottom']) < (line['bottom'] - line['top']) * epsilon


def create_simple_lines(words: List[Word], params: Dict[str, float]) -> List[SimpleLine]:
    lines = []
    for word in words:
        _sorted = False
        for line in lines:
            if belongs_to_line(line, word, params['epsilon']):
                line['x0'] = min(line['x0'], word['x0'])
                line['x1'] = max(line['x1'], word['x1'])
                line['top'] = min(line['top'], word['top'])
                line['bottom'] = max(line['bottom'], word['bottom'])
                line['words'] = line['words'] + [word]
                _sorted = True
                break
        if not _sorted:
            lines.append({
                'x0': word['x0'],
                'x1': word['x1'],
                'top': word['top'],
                'bottom': word['bottom'],
                'words': [word]})
    return lines


def pair_forms_term(pair: Tuple[Word, Word], params: Dict[str, Union[float, str]]) -> bool:
    colon_regex = re.compile(params['colon_regex'])
    mean_word_height = 0.5 * (pair[0]['bottom'] - pair[0]['top'] + pair[1]['bottom'] - pair[1]['top'])
    gap = pair[1]['x0'] - pair[0]['x1']
    return gap / mean_word_height < params['factor'] and not colon_regex.match(pair[0]['text'])


def create_term_lines(simple_lines: List[SimpleLine], params: Dict[str, Union[float, str]]) -> List[Line]:
    terms = []
    for l_i, line in enumerate(simple_lines):
        terms.append({
            'x0': line['x0'],
            'x1': line['x1'],
            'top': line['top'],
            'bottom': line['bottom'],
            'terms': []
        })
        t_i = 0
        words = line['words']
        terms[l_i]['terms'].append({
            'x0': words[0]['x0'],
            'x1': words[0]['x1'],
            'top': words[0]['top'],
            'bottom': words[0]['bottom'],
            'type': 'text',
            'text': words[0]['text'],
            'words': [words[0]]
        })
        if len(words) > 1:
            for pair in zip(words[:-1], words[1:]):
                if pair_forms_term(pair, params):
                    terms[l_i]['terms'][t_i]['x0'] = min(terms[l_i]['terms'][t_i]['x0'], pair[1]['x0'])
                    terms[l_i]['terms'][t_i]['x1'] = max(terms[l_i]['terms'][t_i]['x1'], pair[1]['x1'])
                    terms[l_i]['terms'][t_i]['top'] = min(terms[l_i]['terms'][t_i]['top'], pair[1]['top'])
                    terms[l_i]['terms'][t_i]['bottom'] = max(terms[l_i]['terms'][t_i]['bottom'], pair[1]['bottom'])
                    terms[l_i]['terms'][t_i]['text'] = " ".join([terms[l_i]['terms'][t_i]['text'], pair[1]['text']])
                    terms[l_i]['terms'][t_i]['words'].append(pair[1])
                else:
                    t_i += 1
                    terms[l_i]['terms'].append({
                        'x0': pair[1]['x0'],
                        'x1': pair[1]['x1'],
                        'top': pair[1]['top'],
                        'bottom': pair[1]['bottom'],
                        'type': 'text',
                        'text': pair[1]['text'],
                        'words': [pair[1]]
                    })
    return terms


def belongs_to_same_segment(line: Line, segment: Segment) -> bool:
    return len(line['terms']) == len(segment['lines'][-1]['terms'])


def create_segments(lines: List[Line]) -> List[Segment]:
    seg = []
    seg_id = 0
    seg.append({
        'x0': lines[0]['x0'],
        'x1': lines[0]['x1'],
        'top': lines[0]['top'],
        'bottom': lines[0]['bottom'],
        'shape': tuple(),
        'lines': [lines[0]]
    })
    for line in lines[1:]:
        if belongs_to_same_segment(line, seg[seg_id]):
            seg[seg_id]['x0'] = min(seg[seg_id]['x0'], line['x0'])
            seg[seg_id]['x1'] = max(seg[seg_id]['x1'], line['x1'])
            seg[seg_id]['top'] = min(seg[seg_id]['top'], line['top'])
            seg[seg_id]['bottom'] = max(seg[seg_id]['top'], line['top'])
            seg[seg_id]['lines'].append(line)
        else:
            seg[seg_id]['shape'] = (len(seg[seg_id]['lines']), len(seg[seg_id]['lines'][-1]['terms']))
            seg_id += 1
            seg.append({
                'x0': line['x0'],
                'x1': line['x1'],
                'top': line['top'],
                'bottom': line['bottom'],
                'shape': None,
                'lines': [line]
            })
    seg[seg_id]['shape'] = (len(seg[seg_id]['lines']), len(seg[seg_id]['lines'][-1]['terms']))
    return seg


def get_document_structure(pdf: pdfplumber.PDF, config: ParseConfig) -> Document:
    document = []
    for page_id, page in enumerate(pdf.pages):
        page_words = make_words_float(page.extract_words())
        simple_lines = create_simple_lines(page_words, config['line'])
        term_lines = create_term_lines(simple_lines, config['term'])
        segments = create_segments(term_lines)
        document.append({
            'segments': segments
        })
    return document
