from typing import Pattern, List, Union

from ..types import DocCoord, Document, Term, Line, Segment


def doc_coords_iterator(doc: Document) -> DocCoord:
    for p_i, page in enumerate(doc):
        for s_i, seg in enumerate(page['segments']):
            for l_i, line in enumerate(seg['lines']):
                for t_i, term in enumerate(line['terms']):
                    yield DocCoord(p_i, s_i, l_i, t_i)


def doc_seg(doc: Document, coord: DocCoord) -> Segment:
    return doc[coord.page]['segments'][coord.seg]


def doc_line(doc: Document, coord: DocCoord) -> Line:
    return doc[coord.page]['segments'][coord.seg]['lines'][coord.line]


def doc_term(doc: Document, coord: DocCoord) -> Term:
    return doc[coord.page]['segments'][coord.seg]['lines'][coord.line]['terms'][coord.term]


def get_match_coords(doc: Document, regex: Pattern) -> List[DocCoord]:
    return [coord for coord in doc_coords_iterator(doc) if regex.match(doc_term(doc, coord)['text'].lower())]


def get_match_terms(doc: Document, regex: Pattern) -> List[Term]:
    return [doc_term(doc, coord) for coord in get_match_coords(doc, regex)]


def extract_regex_value(doc: Document, regex: Pattern, rel_coord: DocCoord, term_type: str) -> List[Union[str, float]]:
    regex_coords = get_match_coords(doc, regex)
    results = []
    for regex_coord in regex_coords:
        value_coord = regex_coord + rel_coord
        value_term = doc_term(doc, value_coord)
        if term_type == 'price':
            results.append(value_term['price'])
        else:
            results.append(value_term['text'])
    return results
