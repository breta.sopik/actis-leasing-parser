import re
from typing import List, Pattern

from .document import doc_coords_iterator, doc_seg, doc_term
from ..types import ParseConfig, Document, DocCoord


def segment_is_stacked_even_terms(doc: Document, coord: DocCoord) -> bool:
    seg = doc_seg(doc, coord)
    return (seg['shape'][1] == 1) and (seg['shape'][0] % 2 == 0) and (seg['shape'][0] > 0)


def term_is_col_key(doc: Document, coord: DocCoord, keyword_regexes: List[Pattern]) -> bool:
    if (coord.line % 2 == 0) and (doc_seg(doc, coord)['shape'][0] > coord.line + 1):
        term = doc_term(doc, coord)
        term_matches = [r.match(term['text'].lower()) for r in keyword_regexes]
        next_term = doc_term(doc, coord + DocCoord(0, 0, 1, 0))
        next_term_matches = [r.match(next_term['text'].lower()) for r in keyword_regexes]
        return any(term_matches) and not any(next_term_matches)
    else:
        return False


def label_vertical_key_values(doc: Document, regex_strings: List[str]) -> Document:
    keyword_regexes = [re.compile(s) for s in regex_strings]
    for c in doc_coords_iterator(doc):
        if segment_is_stacked_even_terms(doc, c) and term_is_col_key(doc, c, keyword_regexes):
            doc[c.page]['segments'][c.seg]['lines'][c.line]['terms'][c.term]['type'] = 'key'
            doc[c.page]['segments'][c.seg]['lines'][c.line]['terms'][c.term]['value_coord'] = (0, 0, 1, 0)
            doc[c.page]['segments'][c.seg]['lines'][c.line + 1]['terms'][c.term]['type'] = 'value'
    return doc


def segment_has_even_terms(doc: Document, coord: DocCoord) -> bool:
    seg = doc_seg(doc, coord)
    return (seg['shape'][1] % 2 == 0) and (seg['shape'][0] > 1)


def term_is_row_key(doc: Document, coord: DocCoord, keyword_regexes: List[Pattern]) -> bool:
    if (coord.term % 2 == 0) and (doc_seg(doc, coord)['shape'][1] > coord.term + 1):
        term = doc_term(doc, coord)
        term_matches = [r.match(term['text'].lower()) for r in keyword_regexes]
        next_term = doc_term(doc, coord + DocCoord(0, 0, 0, 1))
        next_term_matches = [r.match(next_term['text'].lower()) for r in keyword_regexes]
        return any(term_matches) and not any(next_term_matches)
    else:
        return False


def label_horizontal_key_values(doc: Document, regex_strings: List[str]) -> Document:
    keyword_regexes = [re.compile(s) for s in regex_strings]
    for c in doc_coords_iterator(doc):
        if segment_has_even_terms(doc, c) and term_is_row_key(doc, c, keyword_regexes):
            doc[c.page]['segments'][c.seg]['lines'][c.line]['terms'][c.term]['type'] = 'key'
            doc[c.page]['segments'][c.seg]['lines'][c.line]['terms'][c.term]['value_coord'] = (0, 0, 0, 1)
            doc[c.page]['segments'][c.seg]['lines'][c.line]['terms'][c.term + 1]['type'] = 'value'
    return doc


def term_is_price(doc: Document, coord: DocCoord, price_regexes: List[Pattern]) -> bool:
    return any(r.match(doc_term(doc, coord)['text']) for r in price_regexes)


def parse_price(doc: Document, coord: DocCoord) -> float:
    try:
        price = float(''.join(doc_term(doc, coord)['text'][:-2].replace(',', '.').split()))
    except:
        price = None
    return price


def label_prices(doc: Document, regex_strings: List[str]) -> Document:
    price_regexes = [re.compile(s) for s in regex_strings]
    for c in doc_coords_iterator(doc):
        if term_is_price(doc, c, price_regexes):
            doc[c.page]['segments'][c.seg]['lines'][c.line]['terms'][c.term]['type'] = 'price'
            doc[c.page]['segments'][c.seg]['lines'][c.line]['terms'][c.term]['price'] = parse_price(doc, c)
    return doc


def label_document(document: Document, params: ParseConfig) -> Document:
    typing_functions = [
        (label_vertical_key_values, params['keywords']),
        (label_horizontal_key_values, params['keywords']),
        (label_prices, params['price'])
    ]
    for func, params in typing_functions:
        document = func(document, params)
    return document
