import pdfplumber

from .structure import get_document_structure
from .labeling import label_document
from ..types import Document, ParseConfig
from ..utils import logger


def parse_document(file: str, config: ParseConfig) -> Document:
    pdf_file = pdfplumber.open(file)
    logger.info(f'Parsing document: {file}')
    return label_document(get_document_structure(pdf_file, config), config)
