import os

from ._version import get_versions
from .utils import load_config

__version__ = get_versions()['version']
del get_versions

default_config_file = os.path.join(os.path.dirname(__file__), 'resources', 'config.yaml')
config = load_config(default_config_file)
