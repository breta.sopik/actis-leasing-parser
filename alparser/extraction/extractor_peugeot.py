import re

import pandas as pd

from .extractor import BaseExtractor, extract_value
from ..matching import match_database_base_price
from ..types import Document, Digest, DocCoord


class PeugeotExtractor(BaseExtractor):
    template_regexes = [
        re.compile(r"^nabídka\sč"),
        re.compile(r"^technické\sparametry"),
        re.compile(r"^sériová\svýbava"),
        re.compile(r"^základní\sceníková\scena"),
        re.compile(r"^barva\slaku\sa\sčalounění"),
        re.compile(r"^cena\spřed\sslevou"),
        re.compile(r"^akční\ssleva"),
        re.compile(r"^nabízená\scena\sbez\sdph"),
        re.compile(r"^nabízená\scena\ss\sdph")
    ]

    params_base_price = {
        'regex': re.compile(r"^základní\sceníková\scena"),
        'rel_coord': DocCoord(0, 0, 0, 1)
    }

    params_manu_price = {
        'regex': re.compile(r"^cena\spřed\sslevou"),
        'rel_coord': DocCoord(0, 0, 0, 1)
    }

    params_reduction = {
        'regex': re.compile(r"^akční\ssleva"),
        'rel_coord': DocCoord(0, 0, 0, 3)
    }

    params_final_price = {
        'regex': re.compile(r"^nabízená\scena\sbez\sdph"),
        'rel_coord': DocCoord(0, 0, 0, 1)
    }

    def extract(self, document: Document, database: pd.DataFrame) -> Digest:
        p = {
            'base_price': extract_value(document, self.params_base_price, 'price'),
            'manu_price': extract_value(document, self.params_manu_price, 'price'),
            'reduction': extract_value(document, self.params_reduction, 'price'),
            'final_price': extract_value(document, self.params_final_price, 'price')
        }
        p['manu_extra'] = p['manu_price'] - p['base_price']
        p['dealer_extra'] = p['final_price'] - p['reduction'] - p['manu_extra'] - p['base_price']

        m = match_database_base_price(document, database, p['base_price'], self.config).to_dict()
        return {**m, **p}
