from typing import List

import pandas as pd

from .extractor import BaseExtractor
from .extractor_citroen import CitroenExtractor
from .extractor_peugeot import PeugeotExtractor
from .extractor_generic import GenericExtractor
from ..types import Document, Digest, MatchConfig
from ..utils import logger


def recognize_template(document: Document, extractors: List[BaseExtractor]) -> List[BaseExtractor]:
    results = []
    for extr in extractors:
        if extr.recognize(document):
            results.append(extr)
    return results


def eval_extractors(extractors: List[BaseExtractor], fallback: BaseExtractor) -> BaseExtractor:
    if len(extractors) > 1:
        raise ValueError('Multiple templates were recognized.')
    elif len(extractors) == 1:
        extr = extractors[0]
        logger.info(f'Document matches template: {type(extr).__name__}')
    else:
        extr = fallback
        logger.info(f'No template matched. Using fallback extractor: {type(extr).__name__}')
    return extr


def extract_document(document: Document,
                     database: pd.DataFrame,
                     config: MatchConfig,
                     library: List[GenericExtractor] = None) -> Digest:
    logger.info('Extracting document.')
    library = [CitroenExtractor(config), PeugeotExtractor(config)] if library is None else library
    extractors = recognize_template(document, library)
    extr = eval_extractors(extractors, GenericExtractor(config))
    return extr.extract(document, database)
