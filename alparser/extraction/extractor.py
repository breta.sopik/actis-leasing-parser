from typing import Dict, Pattern, Union

import pandas as pd

from ..parsing.document import get_match_terms
from ..parsing.document import extract_regex_value
from ..types import Document, Digest, DocCoord, MatchConfig


def extract_value(doc: Document, price_params: Dict[str, Union[Pattern, DocCoord]], type_label: str) -> float:
    return extract_regex_value(doc, price_params['regex'], price_params['rel_coord'], type_label)[0]


class BaseExtractor:
    template_regexes = []

    def __init__(self, config: MatchConfig):
        self.config = config

    def recognize(self, document: Document) -> bool:
        return all(len(get_match_terms(document, r)) > 0 for r in self.template_regexes)

    def extract(self, document: Document, database: pd.DataFrame) -> Digest:
        pass
