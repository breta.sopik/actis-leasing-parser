import re

import pandas as pd

from .extractor import BaseExtractor, extract_value
from ..matching import match_database_base_price
from ..types import Document, Digest, DocCoord


class CitroenExtractor(BaseExtractor):
    template_regexes = [
        re.compile(r"^cenová\snabídka\sč"),
        re.compile(r"^technická\scharakteristika"),
        re.compile(r"^sériová\svýbava"),
        re.compile(r"^základní\sceníková\scena"),
        re.compile(r"^ceníková\scena\svozidla\ss\spříplatkovou\svýbavou"),
        re.compile(r"^výsledná\scenová\snabídka")
    ]

    params_base_price = {
        'regex': re.compile(r"^základní\sceníková\scena"),
        'rel_coord': DocCoord(0, 0, 0, 1)
    }

    params_manu_price = {
        'regex': re.compile(r"^ceníková\scena\svozidla\ss\spříplatkovou\svýbavou"),
        'rel_coord': DocCoord(0, 0, 0, 1)
    }

    params_reduction = {
        'regex': re.compile(r"^ceníková\scena\svozidla\ss\spříplatkovou\svýbavou"),
        'rel_coord': DocCoord(0, 0, 1, 1)
    }

    params_final_price = {
        'regex': re.compile(r"^výsledná\scenová\snabídka"),
        'rel_coord': DocCoord(0, 0, 0, 1)
    }

    def extract(self, document: Document, database: pd.DataFrame) -> Digest:
        p = {
            'base_price': extract_value(document, self.params_base_price, 'price'),
            'manu_price': extract_value(document, self.params_manu_price, 'price'),
            'reduction': extract_value(document, self.params_reduction, 'price'),
            'final_price': extract_value(document, self.params_final_price, 'price')
        }
        p['manu_extra'] = p['manu_price'] - p['base_price']
        p['dealer_extra'] = p['final_price'] - p['reduction'] - p['manu_extra'] - p['base_price']

        m = match_database_base_price(document, database, p['base_price'], self.config).to_dict()
        return {**m, **p}
