import pandas as pd

from .extractor import BaseExtractor
from ..matching import match_database
from ..types import Document, Digest


class GenericExtractor(BaseExtractor):
    def recognize(self, document: Document) -> bool:
        return True

    def extract(self, document: Document, database: pd.DataFrame) -> Digest:
        p = {
            'base_price': None,
            'manu_price': None,
            'reduction': None,
            'final_price': None,
            'manu_extra': None,
            'dealer_extra': None
        }

        m = match_database(document, database, self.config, 1).iloc[0].to_dict()
        return {**m, **p}
