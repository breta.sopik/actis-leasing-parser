from collections import namedtuple
from decimal import Decimal

from typing import Dict, List, Tuple, Union, Pattern

# parsing
DecWord = Dict[str, Union[Decimal, str]]
Word = Dict[str, Union[float, str]]
SimpleLine = Dict[str, Union[int, float, List[Word]]]
Term = Dict[str, Union[int, float, str, Tuple[int, int], List[Word]]]
Line = Dict[str, Union[int, float, List[Term]]]
Segment = Dict[str, Union[int, float, List[Line], Tuple[int, int]]]
Page = Dict[str, Union[int, List[Segment]]]
Document = List[Page]

# matching
KeywordsIDF = List[Dict[str, Union[str, Pattern, float]]]

# config
ParseConfig = Dict[str,
                   Union[Dict[str, Union[str, float]],
                         List[str]]]
MatchConfig = Dict[str,
                   Dict[str,
                        Union[List[str],
                              float,
                              Dict[str, List[str]]]]]
Config = Dict[str, Union[ParseConfig, MatchConfig]]

# extraction
Digest = Dict[str, Union[str, float]]
Matches = List[Digest]
DocCoord = namedtuple('DocCoord', ['page', 'seg', 'line', 'term'], verbose=False)


def doc_coord_add(self: DocCoord, other: DocCoord) -> DocCoord:
    return DocCoord(self.page + other.page, self.seg + other.seg, self.line + other.line, self.term + other.term)


DocCoord.__add__ = doc_coord_add
