import click

from . import default_config_file
from .extraction import extract_document
from .parsing import parse_document
from .matching import match_database
from .utils import load_config, load_database, output_digest, output_matches
from .types import Config, Matches, Digest


@click.command()
@click.option("-f", "--file", help="Path to document (PDF format)", required=True)
@click.option("-d", "--database", help="Database file (CSV format)", required=True)
@click.option("-o", "--output", help="Output file (JSON format)", required=False, default="alparser-output.json")
@click.option("-c", "--config", help="Path to config (YAML format)", required=False, default="")
def extract(file: str, database: str, output: str, config: str):
    config = load_config(default_config_file if config == "" else config)
    digest = _extract(file, database, config)
    output_digest(output, digest)


def _extract(file: str, database: str, config: Config) -> Digest:
    document = parse_document(file, config['parse'])
    pdf_database = load_database(database)
    return extract_document(document, pdf_database, config['match'])


@click.command()
@click.option("-f", "--file", help="Path to document (PDF format)", required=True)
@click.option("-d", "--database", help="Database file (CSV format)", required=True)
@click.option("-o", "--output", help="Output file (JSON format)", required=False, default="alparser-output.json")
@click.option("-c", "--config", help="Path to config (YAML format)", required=False, default="")
@click.option("-n", "--num-results", help="Number of closest matches.", required=False, default=3)
def match(file: str, database: str, output: str, config: str, num_results: int):
    config = load_config(default_config_file if config == "" else config)
    matches = _match(file, database, config, num_results)
    output_matches(output, matches)


def _match(file: str, database: str, config: Config, num_results: int) -> Matches:
    document = parse_document(file, config['parse'])
    pdf_database = load_database(database)
    return match_database(document, pdf_database, config['match'], num_results).to_dict('records')


@click.group()
def entry_point():
    pass


entry_point.add_command(extract)
entry_point.add_command(match)
