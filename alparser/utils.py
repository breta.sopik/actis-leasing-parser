import logging
import json
import yaml

import numpy as np
import pandas as pd

from .types import Digest, Matches


logger = logging.getLogger('alparser')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
logger.addHandler(ch)
del ch


def load_config(config_path) -> dict:
    try:
        with open(config_path, 'r') as config_file:
            logger.debug(f'Loaded config from: {config_path}')
            return yaml.load(config_file, Loader=yaml.SafeLoader)
    except FileNotFoundError:
        logger.debug(f'Could not load config at path: {config_path}')


def load_database(filepath: str) -> pd.DataFrame:
    logger.info(f'Loading database: {filepath}')
    return pd.read_csv(filepath)[['ZNACKA', 'MODEL', 'VERZE', 'IDVERZE', 'CENAZANOVY']]


def pretty_print(document):
    for p_i, page in enumerate(document):
        print(f"=== page:{p_i} ===")
        for s_i, segment in enumerate(page['segments']):
            print(f"segment: {s_i}; shape: {segment['shape']}")
            for l_i, line in enumerate(segment['lines']):
                print(f"\tline: {l_i}")
                for t_i, term in enumerate(line['terms']):
                    print(f"\t\tterm: {t_i}: type: {term['type']}; text: {term['text']}")


def cast_digest(digest: Digest) -> Digest:
    casted_digest = {}
    for key, val in digest.items():
        if isinstance(val, np.floating):
            casted_digest[key] = round(float(val), 2)
        elif isinstance(val, float):
            casted_digest[key] = round(val, 2)
        elif isinstance(val, np.integer):
            casted_digest[key] = int(val)
        else:
            casted_digest[key] = val
    return casted_digest


def output_digest(output_file: str, digest: Digest):
    logger.info(f'Saving digest into: {output_file}')
    with open(output_file, 'w') as f:
        json.dump(cast_digest(digest), f)


def output_matches(output_file: str, matches: Matches):
    logger.info(f'Saving matches into: {output_file}')
    with open(output_file, 'w') as f:
        json.dump([cast_digest(d) for d in matches], f)
