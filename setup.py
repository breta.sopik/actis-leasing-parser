from setuptools import setup, find_packages

import versioneer

cmdclass = versioneer.get_cmdclass()

setup(name='alparser',
      version=versioneer.get_version(),
      description='Actis package for parsing car-leasing documents.',
      long_description='',
      classifiers=[],
      keywords='',
      author='Bretislav Sopik',
      author_email='breta.sopik@gmail.com',
      packages=find_packages(exclude=['tests']),
      include_package_data=True,
      zip_safe=False,
      cmdclass=cmdclass,
      python_requires='>=3.6',
      setup_requires=['versioneer'],
      install_requires=[
          'click',
          'pdfplumber',
          'numpy',
          'pandas',
          'pyyaml'
      ],
      extras_require={
          "test": [
              "pre-commit",
              "pycodestyle",
              "flake8",
              "pytest",
              "pytest-flakes",
              "wheel"
          ]
      },
      entry_points="""
          [console_scripts]
          alparser=alparser.cli:entry_point
      """)
